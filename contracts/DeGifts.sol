// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

contract DeGifts {
    address public owner;
    address payable public target;

    event RequestPayed(
        string ref,
        address payer
    );

    struct Payment {
      address payer;
      uint amount;
      uint createdAt;
      bool isValue;
    }

    mapping(string => Payment) public payments;

    constructor() {
        owner = msg.sender;
    }

    function setTarget(address payable _newTarget) public {
        require(msg.sender == owner, "Function only for owner");
        target = _newTarget;
    }

    function makePayment(string memory _ref)
        public
        payable
    {
        require(target != address(0), "Contract is not ready, it needs target address");
        require(payments[_ref].isValue == false, "Duplicated payment reference error");
        target.transfer(msg.value);
        payments[_ref] = Payment({payer: msg.sender, amount: msg.value, createdAt: block.timestamp, isValue: true});
        emit RequestPayed(_ref, msg.sender);
    }

    function getPayment(string memory _ref) public view returns (uint amount, address payer, uint createdAt, bool isValue) {
        Payment memory payment = payments[_ref];
        return (payment.amount, payment.payer, payment.createdAt, payment.isValue);
    }    

    function getBalance() public view returns (uint256) {
        return address(this).balance;
    }

    function transferBalance(address payable _destiny) public {
        require(msg.sender == owner, "Function only for owner");
        _destiny.transfer(address(this).balance);
    }

    receive() external payable {}
}
