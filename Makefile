app=../app
backend=../web3_listener

copy-contracts:
	echo "copy-contracts is deprecated"

compile:
	npx truffle compile && make copy-contracts

deploy-dev:
	make compile && npx truffle migrate development

deploy-stage:
	make compile && npx truffle migrate --network stage

deploy-matic:
	make compile && npx truffle migrate --network matic

deploy-bsc:
	make compile && npx truffle migrate --network bsc

remix:
	npx remixd -s ./