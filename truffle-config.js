const HDWalletProvider = require('@truffle/hdwallet-provider');
const infuraKey = "fj4jll3k.....";

const fs = require('fs');
let privateKey = '' 
// try {
//   privateKey = fs.readFileSync(".secret").toString().trim();
// } catch (error) {
//   console.log('Error while reading .secret file')  
// }

module.exports = {

  networks: {
    development: {
      host: "127.0.0.1",     // Localhost (default: none)
      port: 7545,            // Standard Ethereum port (default: none)
      network_id: "1337",       // Any network (default: none)
      // provider: () => new HDWalletProvider({ privateKeys: [privateKey], providerOrUrl: 'http://127.0.0.1:7545', p}),
    },

    stage: {
      provider: () => new HDWalletProvider({ privateKeys: [privateKey], providerOrUrl: `https://goerli.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161` }),
      network_id: 5,
      gas: 4000000  
    },

    matic: {
      provider: () => new HDWalletProvider({ privateKeys: [privateKey], providerOrUrl: `wss://matic-mainnet-full-ws.bwarelabs.com` }),
      network_id: 137,
      websockets: true,
      // price: 7,
      gas: 5000000
    },
    
    bsc: {
      provider: () => new HDWalletProvider({ privateKeys: [privateKey], providerOrUrl: `https://bsc-dataseed1.binance.org` }),
      network_id: 56,
      confirmations: 10,
      timeoutBlocks: 200,
      skipDryRun: true,
      gas: 5000000
    },

  },

  // Set default mocha options here, use special reporters etc.
  mocha: {
    // timeout: 100000
  },

  // Configure your compilers
  compilers: {
    solc: {
      version: "0.8.1",    // Fetch exact version from solc-bin (default: truffle's version)
    }
  },

  // Truffle DB is currently disabled by default; to enable it, change enabled: false to enabled: true
  //
  // Note: if you migrated your contracts prior to enabling this field in your Truffle project and want
  // those previously migrated contracts available in the .db directory, you will need to run the following:
  // $ truffle migrate --reset --compile-all

  db: {
    enabled: false
  }
};
